Title: Eggnog… Real Eggnog!
Date: 2017-12-10 16:00
Category: Food
Slug: eggnog

## Ingredients

* 1 dozen fresh eggs
* 2 quarts heavy cream
* 1 pound confectioner's sugar
* 2 cups Bourbon or rye whiskey
* whole nutmeg, for garnish

## Method

Separate the eggs; set aside whites for later.
Beat the yolks until light in color.
While beating continuously, gradually beat in the sugar, then slowly add one cup of the whiskey.
Let stand one hour to "cook" out the yolk flavor.

After one hour, slowly add the other cup of whiskey and the cream while beating continuously.
Cover the mixture and refrigerate a minimum of 3 hours, or overnight.

Beat the egg whites until they hold soft peaks.
Do not overbeat.
Fold the egg whites into the cream/whiskey mixture.
Garnish with fresh grated nutmeg; optionally add brandy or grand marnier to taste.
