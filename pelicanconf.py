#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Ted Strzalkowski'
SITENAME = u'tedski.net'
SITEURL = ''
SITETITLE = u'Ted Strzalkowski'
SITEDESCRIPTION = u'Ted Strzalkowski\'s blog.'
SITELOGO = '//s.gravatar.com/avatar/542b79c685f925d19cebef01d517c75e?s=120'
PYGMENTS_STYLE = 'monokai'
TYPOGRIFY = True

OUTPUT_PATH = 'public'
PATH = 'content'
STATIC_PATHS = ['extra/favicon.ico']
EXTRA_PATH_METADATA = {
        'extra/favicon.ico': {'path': 'favicon.ico'},
}
THEME = '../pelican-themes/Flex'
PLUGIN_PATHS = ['/Users/tedski/src/pelican-plugins']

TIMEZONE = 'America/Los_Angeles'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('linkedin', 'https://www.linkedin.com/in/tedski'),
        ('github', 'https://github.com/tedski'),
        ('envelope-o', 'mailto:contact@tedski.net'),
        ('rss', '//tedski.net/feeds/all.atom.xml'))

DEFAULT_PAGINATION = 5

COPYRIGHT_YEAR = 2017


# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
